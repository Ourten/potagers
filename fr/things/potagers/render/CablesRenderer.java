package fr.things.potagers.render;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import fr.things.potagers.tiles.TileEntityCable;

public class CablesRenderer extends TileEntitySpecialRenderer
{
	@Override
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) 
	{
		renderCable((TileEntityCable) tile, x, y ,z);
	}
	
	public void renderCable(TileEntityCable tile, double x, double y, double z)
	{
		
	}
}
package fr.things.potagers.render.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class ModelTomate extends ModelBase
{
	ModelRenderer sol;
    ModelRenderer tuteur;
    ModelRenderer leave2;
    ModelRenderer leave21;
    ModelRenderer leave22;
    ModelRenderer branche1;
    ModelRenderer branche11;
    ModelRenderer branche12;
    ModelRenderer branche13;
    ModelRenderer plant;
    ModelRenderer plant1;
    ModelRenderer plant2;
    ModelRenderer plant3;
    ModelRenderer plant4;
    ModelRenderer plant5;
    ModelRenderer tomate;
    ModelRenderer tomate1;
    ModelRenderer leave1;
    ModelRenderer leave11;
    ModelRenderer leave12;
    ModelRenderer leave13;
    ModelRenderer leave14;
    ModelRenderer leave15;
    ModelRenderer leave16;
    ModelRenderer leave17;
    ModelRenderer sol1;
    ModelRenderer sol2;

	public ModelTomate()
	{
		this( 0.0f );
	}

	public ModelTomate( float par1 )
	{
        sol = new ModelRenderer( this, 52, 52 );
        sol.setTextureSize( 128, 64 );
        sol.addBox( -4F, 0F, -4F, 8, 1, 8);
        sol.setRotationPoint( 0F, 24F, 0F );
        tuteur = new ModelRenderer( this, 0, 25 );
        tuteur.setTextureSize( 128, 64 );
        tuteur.addBox( -1.5F, -32F, -1.5F, 3, 32, 3);
        tuteur.setRotationPoint( 0F, 24F, 0F );
        leave2 = new ModelRenderer( this, 7, 0 );
        leave2.setTextureSize( 128, 64 );
        leave2.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave2.setRotationPoint( 0F, 24F, 0F );
        leave21 = new ModelRenderer( this, 7, 0 );
        leave21.setTextureSize( 128, 64 );
        leave21.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave21.setRotationPoint( 0F, 22F, 0F );
        leave22 = new ModelRenderer( this, 0, -3 );
        leave22.setTextureSize( 128, 64 );
        leave22.addBox( 0F, -3F, -1.5F, 0, 4, 3);
        leave22.setRotationPoint( 0F, 20F, 0F );
        branche1 = new ModelRenderer( this, 13, 1 );
        branche1.setTextureSize( 128, 64 );
        branche1.addBox( -1F, -4F, -1F, 2, 4, 2);
        branche1.setRotationPoint( 0F, 24F, 0F );
        branche11 = new ModelRenderer( this, 13, 1 );
        branche11.setTextureSize( 128, 64 );
        branche11.addBox( -1F, -4F, -1F, 2, 4, 2);
        branche11.setRotationPoint( 0F, 20F, 0F );
        branche12 = new ModelRenderer( this, 13, 1 );
        branche12.setTextureSize( 128, 64 );
        branche12.addBox( -1F, -4F, -1F, 2, 4, 2);
        branche12.setRotationPoint( 0F, 16F, 0F );
        branche13 = new ModelRenderer( this, 13, 1 );
        branche13.setTextureSize( 128, 64 );
        branche13.addBox( -1F, -4F, -1F, 2, 4, 2);
        branche13.setRotationPoint( 0F, 12F, 0F );
        plant = new ModelRenderer( this, 13, 0 );
        plant.setTextureSize( 128, 64 );
        plant.addBox( -1F, -5F, -1F, 2, 5, 2);
        plant.setRotationPoint( 0F, 24F, 0F );
        plant1 = new ModelRenderer( this, 13, 0 );
        plant1.setTextureSize( 128, 64 );
        plant1.addBox( -1F, -5F, -1F, 2, 5, 2);
        plant1.setRotationPoint( 0F, 19F, 0F );
        plant2 = new ModelRenderer( this, 13, 0 );
        plant2.setTextureSize( 128, 64 );
        plant2.addBox( -1F, -5F, -1F, 2, 5, 2);
        plant2.setRotationPoint( 0F, 14F, 0F );
        plant3 = new ModelRenderer( this, 13, 0 );
        plant3.setTextureSize( 128, 64 );
        plant3.addBox( -1F, -5F, -1F, 2, 5, 2);
        plant3.setRotationPoint( 0F, 9F, 0F );
        plant4 = new ModelRenderer( this, 13, 0 );
        plant4.setTextureSize( 128, 64 );
        plant4.addBox( -1F, -5F, -1F, 2, 5, 2);
        plant4.setRotationPoint( 0F, 4F, 0F );
        plant5 = new ModelRenderer( this, 13, 0 );
        plant5.setTextureSize( 128, 64 );
        plant5.addBox( -1F, -5F, -1F, 2, 5, 2);
        plant5.setRotationPoint( 0F, -1F, 0F );
        tomate = new ModelRenderer( this, 36, 0 );
        tomate.setTextureSize( 128, 64 );
        tomate.addBox( -2.5F, -2.5F, -1F, 5, 5, 0);
        tomate.setRotationPoint( 0F, -3.8F, 0F );
        tomate1 = new ModelRenderer( this, 22, 0 );
        tomate1.setTextureSize( 128, 64 );
        tomate1.addBox( -3F, -3F, -3.25F, 6, 6, 6);
        tomate1.setRotationPoint( 0, 0, 0 );
        leave1 = new ModelRenderer( this, 7, 0 );
        leave1.setTextureSize( 128, 64 );
        leave1.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave1.setRotationPoint( 0F, 24F, 0F );
        leave11 = new ModelRenderer( this, 7, 0 );
        leave11.setTextureSize( 128, 64 );
        leave11.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave11.setRotationPoint( 0F, 22F, 0F );
        leave12 = new ModelRenderer( this, 7, 0 );
        leave12.setTextureSize( 128, 64 );
        leave12.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave12.setRotationPoint( 0F, 20F, 0F );
        leave13 = new ModelRenderer( this, 7, 0 );
        leave13.setTextureSize( 128, 64 );
        leave13.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave13.setRotationPoint( 0F, 18F, 0F );
        leave14 = new ModelRenderer( this, 0, -3 );
        leave14.setTextureSize( 128, 64 );
        leave14.addBox( 0F, -3F, -1.5F, 0, 4, 3);
        leave14.setRotationPoint( 0F, 16F, 0F );
        leave15 = new ModelRenderer( this, 7, 0 );
        leave15.setTextureSize( 128, 64 );
        leave15.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave15.setRotationPoint( 0F, 20F, 0F );
        leave16 = new ModelRenderer( this, 7, 0 );
        leave16.setTextureSize( 128, 64 );
        leave16.addBox( -0.5F, -2F, -0.5F, 1, 2, 1);
        leave16.setRotationPoint( 0F, 20F, -2F );
        leave17 = new ModelRenderer( this, 0, -3 );
        leave17.setTextureSize( 128, 64 );
        leave17.addBox( 0F, -3F, -1.5F, 0, 4, 3);
        leave17.setRotationPoint( 0F, 20F, -4F );
        sol1 = new ModelRenderer( this, 22, 50 );
        sol1.setTextureSize( 128, 64 );
        sol1.addBox( -5F, -0.25F, -5F, 10, 1, 10);
        sol1.setRotationPoint( 0F, 25F, 0F );
        sol2 = new ModelRenderer( this, 0, 28 );
        sol2.setTextureSize( 128, 64 );
        sol2.addBox( -16F, 0.45F, -16F, 32, 1, 32);
        sol2.setRotationPoint( 0F, 25F, 0F );
	}

	public void renderStick(float par7)
	{
		tuteur.rotateAngleX = 0F;
	    tuteur.rotateAngleY = 0F;
	    tuteur.rotateAngleZ = 0F;
	    tuteur.renderWithRotation(par7);

	}
	
	public void renderTomate(float par7)
	{
		tomate1.rotateAngleX = 1.570796F;
	    tomate1.rotateAngleY = 0F;
	    tomate1.rotateAngleZ = 0F;
	    tomate1.renderWithRotation(par7);

	}
	
	//Dans cet ordre la le nom.
	public void renderTomateFeuille(float par7)
	{
        tomate.rotateAngleX = 1.570796F;
        tomate.rotateAngleY = 0F;
        tomate.rotateAngleZ = 0F;
        tomate.renderWithRotation(par7);
	}

	public void render(float par7)
	{
		sol.rotateAngleX = 0F;
	        sol.rotateAngleY = 0F;
	        sol.rotateAngleZ = 0F;
	        sol.renderWithRotation(par7);

	        leave2.rotateAngleX = 0F;
	        leave2.rotateAngleY = 0F;
	        leave2.rotateAngleZ = 0F;
	        leave2.renderWithRotation(par7);

	        leave21.rotateAngleX = 0F;
	        leave21.rotateAngleY = 0F;
	        leave21.rotateAngleZ = 0F;
	        leave21.renderWithRotation(par7);

	        leave22.rotateAngleX = 0F;
	        leave22.rotateAngleY = 0F;
	        leave22.rotateAngleZ = 0F;
	        leave22.renderWithRotation(par7);

	        branche1.rotateAngleX = 0F;
	        branche1.rotateAngleY = 0F;
	        branche1.rotateAngleZ = 0F;
	        branche1.renderWithRotation(par7);

	        branche11.rotateAngleX = 0F;
	        branche11.rotateAngleY = 0F;
	        branche11.rotateAngleZ = 0F;
	        branche11.renderWithRotation(par7);

	        branche12.rotateAngleX = 0F;
	        branche12.rotateAngleY = 0F;
	        branche12.rotateAngleZ = 0F;
	        branche12.renderWithRotation(par7);

	        branche13.rotateAngleX = 0F;
	        branche13.rotateAngleY = 0F;
	        branche13.rotateAngleZ = 0F;
	        branche13.renderWithRotation(par7);

	        plant.rotateAngleX = 0F;
	        plant.rotateAngleY = 0F;
	        plant.rotateAngleZ = 0F;
	        plant.renderWithRotation(par7);

	        plant1.rotateAngleX = 0F;
	        plant1.rotateAngleY = 0F;
	        plant1.rotateAngleZ = 0F;
	        plant1.renderWithRotation(par7);

	        plant2.rotateAngleX = 0F;
	        plant2.rotateAngleY = 0F;
	        plant2.rotateAngleZ = 0F;
	        plant2.renderWithRotation(par7);

	        plant3.rotateAngleX = 0F;
	        plant3.rotateAngleY = 0F;
	        plant3.rotateAngleZ = 0F;
	        plant3.renderWithRotation(par7);

	        plant4.rotateAngleX = 0F;
	        plant4.rotateAngleY = 0F;
	        plant4.rotateAngleZ = 0F;
	        plant4.renderWithRotation(par7);

	        plant5.rotateAngleX = 0F;
	        plant5.rotateAngleY = 0F;
	        plant5.rotateAngleZ = 0F;
	        plant5.renderWithRotation(par7);

	        leave1.rotateAngleX = 0F;
	        leave1.rotateAngleY = 0F;
	        leave1.rotateAngleZ = 0F;
	        leave1.renderWithRotation(par7);

	        leave11.rotateAngleX = 0F;
	        leave11.rotateAngleY = 0F;
	        leave11.rotateAngleZ = 0F;
	        leave11.renderWithRotation(par7);

	        leave12.rotateAngleX = 0F;
	        leave12.rotateAngleY = 0F;
	        leave12.rotateAngleZ = 0F;
	        leave12.renderWithRotation(par7);

	        leave13.rotateAngleX = 0F;
	        leave13.rotateAngleY = 0F;
	        leave13.rotateAngleZ = 0F;
	        leave13.renderWithRotation(par7);

	        leave14.rotateAngleX = 0F;
	        leave14.rotateAngleY = 0F;
	        leave14.rotateAngleZ = 0F;
	        leave14.renderWithRotation(par7);

	        leave15.rotateAngleX = 1.570796F;
	        leave15.rotateAngleY = 0F;
	        leave15.rotateAngleZ = 0F;
	        leave15.renderWithRotation(par7);

	        leave16.rotateAngleX = 1.570796F;
	        leave16.rotateAngleY = 0F;
	        leave16.rotateAngleZ = 0F;
	        leave16.renderWithRotation(par7);

	        leave17.rotateAngleX = 1.570796F;
	        leave17.rotateAngleY = 0F;
	        leave17.rotateAngleZ = 0F;
	        leave17.renderWithRotation(par7);
	        
	        sol1.rotateAngleX = 0F;
	        sol1.rotateAngleY = 0F;
	        sol1.rotateAngleZ = 0F;
	        sol1.renderWithRotation(par7);

	        sol2.rotateAngleX = 0F;
	        sol2.rotateAngleY = 0F;
	        sol2.rotateAngleZ = 0F;
	        sol2.renderWithRotation(par7);


	}

}

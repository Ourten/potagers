package fr.things.potagers.render;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import fr.things.potagers.render.models.ModelTomate;
import fr.things.potagers.tiles.TileEntityTomate;

public class TomatesRenderer extends TileEntitySpecialRenderer
{
	@Override
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) 
	{
		renderTomates((TileEntityTomate) tile, x, y ,z);
	}

	public void renderTomates(TileEntityTomate tile, double x, double y, double z)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((float)x+0.5f, (float)y+0.815f, (float)z+0.5f);
		GL11.glScalef(0.5f, 0.5f, 0.5f);
		GL11.glRotatef(180f, 1f, 0f, 0f);
		this.bindTexture(new ResourceLocation("potagers","textures/blocks/model_tomate.png"));
		new ModelTomate().render(0.0625f);

		GL11.glScalef(1f, 1f, 1f);
		new ModelTomate().renderStick(0.0625f);
		GL11.glPopMatrix();


		for(int i = 0; i<tile.getIndexLength();i++)
		{
			GL11.glPushMatrix();

			GL11.glTranslatef((float)x+tile.getX(i), (float)y+tile.getY(i), (float)z+tile.getZ(i));
			GL11.glRotatef(180f, 1f, 0f, 0f);

			GL11.glScalef(.75f, .75f, .75f);
			GL11.glScalef((float)tile.getMaturationLevel(i)/50, (float)tile.getMaturationLevel(i)/50, (float)tile.getMaturationLevel(i)/50);
			GL11.glColor4d(0d, 0.6d, 0d, 1d);
			new ModelTomate().renderTomateFeuille(0.0625f);

			tile.genColor(i);
			new ModelTomate().renderTomate(0.0625f);

			GL11.glPopMatrix();
		}
	}
}
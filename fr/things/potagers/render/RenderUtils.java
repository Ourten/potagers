package fr.things.potagers.render;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.IIcon;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

import org.lwjgl.opengl.GL11;

public class RenderUtils 
{
	public static final void renderFluid(TileEntityRendererDispatcher dis, Fluid fluid)
	{
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_LIGHTING);

		IIcon texture = fluid.getStillIcon();
		bindTexture(getFluidSheet(fluid),dis);
		int color = fluid.getColor();

		Tessellator t = Tessellator.instance;

		double ySouthEast = 1;
		double yNorthEast = 1;
		double ySouthWest = 1;
		double yNorthWest = 1;

		double uMin = texture.getInterpolatedU(0.0);
		double uMax = texture.getInterpolatedU(16.0);
		double vMin = texture.getInterpolatedV(0.0);
		double vMax = texture.getInterpolatedV(16.0);

		double vHeight = vMax - vMin;

		float r = (color >> 16 & 0xFF) / 255.0F;
		float g = (color >> 8 & 0xFF) / 255.0F;
		float b = (color & 0xFF) / 255.0F;

		// north side
		t.startDrawingQuads();
		t.setColorOpaque_F(r, g, b);
		t.addVertexWithUV(0.5, -0.5, -0.5, uMax, vMin); // bottom
		t.addVertexWithUV(-0.5, -0.5, -0.5, uMin, vMin); // bottom
		t.addVertexWithUV(-0.5, -0.5 + yNorthWest, -0.5, uMin, vMin
				+ (vHeight * yNorthWest)); // top
											// north/west
		t.addVertexWithUV(0.5, -0.5 + yNorthEast, -0.5, uMax, vMin
				+ (vHeight * yNorthEast)); // top
											// north/east
		t.draw();

		// south side
		t.startDrawingQuads();
		t.setColorOpaque_F(r, g, b);
		t.addVertexWithUV(0.5, -0.5, 0.5, uMin, vMin);
		t.addVertexWithUV(0.5, -0.5 + ySouthEast, 0.5, uMin, vMin
				+ (vHeight * ySouthEast)); // top
											// south
											// east
		t.addVertexWithUV(-0.5, -0.5 + ySouthWest, 0.5, uMax, vMin
				+ (vHeight * ySouthWest)); // top
											// south
											// west
		t.addVertexWithUV(-0.5, -0.5, 0.5, uMax, vMin);
		t.draw();

		// east side
		t.startDrawingQuads();
		t.setColorOpaque_F(r, g, b);
		t.addVertexWithUV(0.5, -0.5, -0.5, uMin, vMin);
		t.addVertexWithUV(0.5, -0.5 + yNorthEast, -0.5, uMin, vMin
				+ (vHeight * yNorthEast)); // top
											// north/east
		t.addVertexWithUV(0.5, -0.5 + ySouthEast, 0.5, uMax, vMin
				+ (vHeight * ySouthEast)); // top
											// south/east
		t.addVertexWithUV(0.5, -0.5, 0.5, uMax, vMin);
		t.draw();

		// west side
		t.startDrawingQuads();
		t.setColorOpaque_F(r, g, b);
		t.addVertexWithUV(-0.5, -0.5, 0.5, uMin, vMin);
		t.addVertexWithUV(-0.5, -0.5 + ySouthWest, 0.5, uMin, vMin
				+ (vHeight * ySouthWest)); // top
											// south/west
		t.addVertexWithUV(-0.5, -0.5 + yNorthWest, -0.5, uMax, vMin
				+ (vHeight * yNorthWest)); // top
											// north/west
		t.addVertexWithUV(-0.5, -0.5, -0.5, uMax, vMin);
		t.draw();

		// top
		t.startDrawingQuads();
		t.setColorOpaque_F(r, g, b);
		t.addVertexWithUV(0.5, -0.5 + ySouthEast, 0.5, uMax, vMin); // south
																	// east
		t.addVertexWithUV(0.5, -0.5 + yNorthEast, -0.5, uMin, vMin); // north
																		// east
		t.addVertexWithUV(-0.5, -0.5 + yNorthWest, -0.5, uMin, vMax); // north
																		// west
		t.addVertexWithUV(-0.5, -0.5 + ySouthWest, 0.5, uMax, vMax); // south
																		// west
		t.draw();

		// bottom
		t.startDrawingQuads();
		t.setColorOpaque_F(r, g, b);
		t.addVertexWithUV(0.5, -0.5, -0.5, uMax, vMin);
		t.addVertexWithUV(0.5, -0.5, 0.5, uMin, vMin);
		t.addVertexWithUV(-0.5, -0.5, 0.5, uMin, vMax);
		t.addVertexWithUV(-0.5, -0.5, -0.5, uMax, vMax);
		t.draw();
		
		GL11.glEnable(2896);
		GL11.glPopMatrix();
	}
	
	public static ResourceLocation getFluidSheet(FluidStack liquid) 
	{
		if (liquid == null) return TextureMap.locationBlocksTexture;
		return getFluidSheet(liquid.getFluid());
	}

	public static ResourceLocation getFluidSheet(Fluid liquid) 
	{
		return TextureMap.locationBlocksTexture;
	}
	
    public static final void bindTexture(ResourceLocation loc, TileEntityRendererDispatcher dis)
    {
        TextureManager texturemanager = dis.field_147553_e;

        if (texturemanager != null)
        {
            texturemanager.bindTexture(loc);
        }
    }
}
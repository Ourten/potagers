package fr.things.potagers;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import fr.things.potagers.blocks.BlockSmallPump;
import fr.things.potagers.blocks.BlockTomate;
import fr.things.potagers.blocks.BlockWaterConduit;
import fr.things.potagers.tiles.TileEntityCable;
import fr.things.potagers.tiles.TileEntityTomate;

@Mod(modid = Potagers.MOD_ID, name = Potagers.MOD_NAME, version = Potagers.MOD_VERSION)
public class Potagers 
{
	//Constants
	public static final String MOD_ID = "Potagers";
	public static final String MOD_VERSION = "Alpha 0.1";
	public static final String MOD_NAME = "Potagers Mod";
	
	public static final CreativeTabs tabPotagers = new CreativeTabPotagers("Potagers");
	
	@SidedProxy(clientSide = "fr.things.potagers.ClientProxy", serverSide = "fr.things.potagers.CommonProxy")
    public static CommonProxy proxy;
    @Instance("Potagers")
    public static Potagers instance;

	//Blocks
	public static Block blockChoux;
	public static Block blockNavets;
	public static Block blockAubergines;
	public static Block blockCourgettes;
	public static Block blockOignons;

	public static Block blockFlowers;

	public static Block blockVignes;
	public static Block blockPechers;
	public static Block blockFraisiers;
	public static Block blockPoiriers;
	public static Block blockPommiers;
	public static Block blockTomates;

	public static Block blockArroseurs;
	public static Block blockWaterConduit;
	public static Block blockSmallPump;

	//Items
	public static Item itemLegumes;
	public static Item itemFruits;


	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		instance = this;
		
		//Blocks Declaration
		blockTomates =  new BlockTomate();
		//blockChoux = new BlockChou();
		//blockNavets = new BlockNavet();
		//blockAubergines = new BlockAubergine();
		//blockCourgettes = new BlockCourgette();
		//blockOignons = new BlockOignon();
		//blockFlowers = new BlockFlowers();
		//blockVignes = new BlockVigne();
		//blockPechers = new BlockPecher();
		//blockFraisiers = new BlockFraisier();
		//blockPoiriers = new BlockPoirier();
		//blockPommiers = new BlockPommier();
		//blockArroseurs = new BlockArroseur();
		blockWaterConduit = new BlockWaterConduit();
		blockSmallPump = new BlockSmallPump();

		//Items Declaration
		//itemLegumes = new ItemLegumes();
		//itemFruits = new ItemFruits();
		
		//Blocks Registry
		GameRegistry.registerBlock(blockTomates, blockTomates.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(blockWaterConduit, blockWaterConduit.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(blockSmallPump, blockSmallPump.getUnlocalizedName().substring(5));
		
		GameRegistry.registerTileEntity(TileEntityTomate.class, "TileEntityTomate");
		GameRegistry.registerTileEntity(TileEntityCable.class, "TileEntityCable");
		
		proxy.registerRenders();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
	}
}
package fr.things.potagers.tiles;

import net.minecraft.nbt.NBTTagCompound;


public class ProductObject 
{
	private float x;
	private float y;
	private float z;
	
	private int maturationLevel;
	private int maturationMax;
	
	public ProductObject(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public float getX()
	{
		return this.x;
	}
	
	public float getY()
	{
		return this.y;
	}
	
	public float getZ()
	{
		return this.z;
	}
	
	public int getMaturationLevel()
	{
		return this.maturationLevel;
	}
	
	public int getMaturationMax()
	{
		return this.maturationMax;
	}
	
	public void setX(float i)
	{
		this.x = i;
	}
	
	public void setY(float i)
	{
		this.y = i;
	}
	
	public void setZ(float i)
	{
		this.z = i;
	}
	
	public void setMaturationLevel(int i)
	{
		this.maturationLevel = i;
	}
	
	public void setMaturationMax(int i)
	{
		this.maturationMax = i;
	}
	
	public ProductObject readFromNBT(NBTTagCompound tag, String key)
	{
		ProductObject p = new ProductObject(tag.getFloat("Px"+key), tag.getFloat("Py"+key), tag.getFloat("Pz"+key));
		
		p.setMaturationLevel(tag.getInteger("Pmlv"+key));
		p.setMaturationMax(tag.getInteger("Pmmx"+key));
		
		return p;
	}
	
	public void writeToNBT(NBTTagCompound tag, String key)
	{
		tag.setFloat("Px"+key, this.getX());
		tag.setFloat("Py"+key, this.getY());
		tag.setFloat("Pz"+key, this.getZ());
		
		tag.setInteger("Pmlv"+key, this.getMaturationLevel());
		tag.setInteger("Pmmx"+key, this.getMaturationMax());
	}
}
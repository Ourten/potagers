package fr.things.potagers.tiles;

import java.util.Random;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import cpw.mods.fml.common.FMLCommonHandler;

public abstract class TileEntityPlants extends TileEntity implements IMaturablePlant
{
	protected ProductObject[] prods;
	
	public TileEntityPlants()
	{
		initialize();
	}
	
	@Override
	public void updateEntity()
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isServer())
		{
			if(new Random().nextInt(10) == 3)
			{
				this.incrementMaturation(0, 1);
				this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
			}
		}
	}
	
	@Override
    public Packet getDescriptionPacket()
    {
        NBTTagCompound nbtTag = new NBTTagCompound();
        this.writeToNBT(nbtTag);
        return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet)
    {
        readFromNBT(packet.func_148857_g());
    }
    
	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);

		//Products
		tag.setInteger("nbrP", prods.length);

		for(int i =0;i<this.prods.length;i++)
		{
			this.prods[i].writeToNBT(tag, "K"+i);
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);

		this.prods = new ProductObject[tag.getInteger("nbrP")];

		int i = 0;
		for(@SuppressWarnings("unused") ProductObject p : this.prods)
		{
			this.prods[i] = new ProductObject(0,0,0).readFromNBT(tag, "K"+i);;
			i=i+1;
		}
	}

	@Override
	public int getMaturationLevel(int index) 
	{
		if(this.prods.length>=index)
			return this.prods[index].getMaturationLevel();
		else
			return 100;
	}

	@Override
	public int getMaxMaturation(int index) 
	{
		if(this.prods.length>=index)
			return this.prods[index].getMaturationMax();
		else
			return 100;
	}

	@Override
	public void setMaturationLevel(int index, int level) 
	{
		if(this.prods.length>=index)
			this.prods[index].setMaturationLevel(level);
		return;
	}

	@Override
	public void setMaxMaturation(int index, int max) 
	{
		if(this.prods.length>=index)
			this.prods[index].setMaturationMax(max);
		return;
	}

	@Override
	public int getIndexLength()
	{
		return this.prods.length;
	}

	@Override
	public void incrementMaturation(int index, int i) 
	{
		if(this.getMaturationLevel(index)+i > this.getMaxMaturation(index))
			return;
		this.setMaturationLevel(index, this.getMaturationLevel(index)+i);
	}

	@Override
	public boolean canMaturate(int index) 
	{
		if(this.getMaturationLevel(index) < this.getMaxMaturation(index))
			return true;
		return false;
	}
	
	
	// ==========================
	//          Position
	// ==========================
	
	@Override
	public float getX(int index) 
	{
		if(this.prods.length>=index)
			return this.prods[index].getX();
		return 0f;
	}

	@Override
	public float getY(int index) 
	{
		if(this.prods.length>=index)
			return this.prods[index].getY();
		return 0f;
	}

	@Override
	public float getZ(int index) 
	{
		if(this.prods.length>=index)
			return this.prods[index].getZ();
		return 0f;
	}

	@Override
	public void setPos(int index, float x, float y, float z) 
	{
		this.prods[index].setX(x);
		this.prods[index].setY(y);
		this.prods[index].setZ(z);
	}
	
	@Override
	public void initialize()
	{
		this.prods = new ProductObject[4];
		
		for(int i = 0;i<prods.length;i++)
		{
			this.prods[i] = new ProductObject(0.6f,0.75f,0.6f);
			this.setMaxMaturation(i, 50);
		}
	}
}
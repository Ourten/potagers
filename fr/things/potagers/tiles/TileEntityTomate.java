package fr.things.potagers.tiles;

import org.lwjgl.opengl.GL11;

public class TileEntityTomate extends TileEntityPlants
{
	public TileEntityTomate() {}
	
	public void genColor(int index)
	{
		float value = (float)this.getMaturationLevel(index)/this.getMaxMaturation(index);
		if(value<0.4f)
			GL11.glColor4d(0d, 1d, 0d, 1d);
		if(value>=0.4f && value <= 0.6)
			GL11.glColor4d(value,1d-value/2,0d,1d);
		if(value>=0.6 && value<1)
			GL11.glColor4d(value+0.15,1d-value/2,0d,1d);
		if(this.getMaturationLevel(index) >= this.getMaxMaturation(index))
			GL11.glColor4d(1d, 0d, 0d, 1d);
	}
}
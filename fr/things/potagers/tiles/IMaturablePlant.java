package fr.things.potagers.tiles;

public interface IMaturablePlant 
{
	int getMaturationLevel(int index);
	int getMaxMaturation(int index);
	float getX(int index);
	float getY(int index);
	float getZ(int index);
	
	void setMaturationLevel(int index, int level);
	void setMaxMaturation(int index, int max);
	void setPos(int index, float x, float y, float z);
	
	int getIndexLength();
	
	void incrementMaturation(int index, int i);
	
	boolean canMaturate(int index);
	
	void initialize();
}
package fr.things.potagers.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;

public abstract class BlockPotagersPlant extends BlockContainer implements IPlantable
{
	public BlockPotagersPlant(Material p_i45386_1_) 
	{
		super(p_i45386_1_);
		this.setTickRandomly(true);
	}
	
	@Override
	public boolean shouldSideBeRendered(IBlockAccess access, int x, int y, int z, int side)
	{
		return false;
	}
	
	@Override
    public boolean isOpaqueCube()
    {
        return false;
    }

	@Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }
    
	@Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World w, int x, int y, int z)
    {
        return null;
    }
    
	@Override
    public boolean canPlaceBlockAt(World w, int x, int y, int z)
    {
        return super.canPlaceBlockAt(w, x, y, z) && this.canBlockStay(w, x, y, z);
    }

	@Override
    public void onNeighborBlockChange(World w, int p_149695_2_, int p_149695_3_, int p_149695_4_, Block p_149695_5_)
    {
        super.onNeighborBlockChange(w, p_149695_2_, p_149695_3_, p_149695_4_, p_149695_5_);
        this.checkAndDropBlock(w, p_149695_2_, p_149695_3_, p_149695_4_);
    }

	@Override
    public void updateTick(World w, int p_149674_2_, int p_149674_3_, int p_149674_4_, Random p_149674_5_)
    {
        this.checkAndDropBlock(w, p_149674_2_, p_149674_3_, p_149674_4_);
    }

    protected void checkAndDropBlock(World w, int p_149855_2_, int p_149855_3_, int p_149855_4_)
    {
        if (!this.canBlockStay(w, p_149855_2_, p_149855_3_, p_149855_4_))
        {
            this.dropBlockAsItem(w, p_149855_2_, p_149855_3_, p_149855_4_, w.getBlockMetadata(p_149855_2_, p_149855_3_, p_149855_4_), 0);
            w.setBlock(p_149855_2_, p_149855_3_, p_149855_4_, getBlockById(0), 0, 2);
        }
    }

    public boolean canBlockStay(World w, int x, int y, int z)
    {
        return w.getBlock(x, y - 1, z) == Blocks.farmland;
    }
}
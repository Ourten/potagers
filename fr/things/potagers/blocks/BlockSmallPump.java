package fr.things.potagers.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import fr.things.potagers.Potagers;
import fr.things.potagers.tiles.TileEntitySmallPump;

public class BlockSmallPump extends BlockContainer
{
	public BlockSmallPump()
	{
		super(Material.rock);
		this.setCreativeTab(Potagers.tabPotagers);
		this.setBlockName("smallpump");
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) 
	{
		return new TileEntitySmallPump();
	}
}
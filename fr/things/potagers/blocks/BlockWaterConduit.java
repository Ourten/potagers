package fr.things.potagers.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import fr.things.potagers.Potagers;
import fr.things.potagers.tiles.TileEntityCable;

public class BlockWaterConduit extends BlockContainer
{
	public BlockWaterConduit() 
	{
		super(Material.wood);
		this.setCreativeTab(Potagers.tabPotagers);
		this.setBlockName("water conduit");
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) 
	{
		return new TileEntityCable();
	}
}
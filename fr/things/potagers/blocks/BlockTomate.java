package fr.things.potagers.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import fr.things.potagers.Potagers;
import fr.things.potagers.tiles.TileEntityTomate;

public class BlockTomate extends BlockPotagersPlant
{
	public BlockTomate() 
	{
		super(Material.plants);
		this.setCreativeTab(Potagers.tabPotagers);
		this.setBlockName("tomates");
	}

	@Override
	public EnumPlantType getPlantType(IBlockAccess world, int x, int y, int z) 
	{
		return EnumPlantType.Plains;
	}

	@Override
	public Block getPlant(IBlockAccess world, int x, int y, int z) 
	{
		return this;
	}

	@Override
	public int getPlantMetadata(IBlockAccess world, int x, int y, int z) 
	{
		return 0;
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) 
	{
		return new TileEntityTomate();
	}

	/*public int damageDropped(int par1)
	{
		return par1;
	}

	@SideOnly(Side.CLIENT)
	private IIcon[] icons;

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
		icons = new IIcon[4];

		for(int i = 0; i < icons.length; i++)
		{
			icons[i] = par1IconRegister.registerIcon(Potagers.MOD_ID + ":" + (this.getUnlocalizedName().substring(5)) + i);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int par1, int par2)
	{
		switch(par2)
		{
		case 0:
			return icons[0];
		case 1:
		{
			switch(par1)
			{
			case 0:
				return icons[1];
			case 1:
				return icons[2];
			default:
				return icons[3];
			}
		}
		default:
		{
			System.out.println("Invalid metadata for " + this.getUnlocalizedName());
					return icons[0];
		}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubBlocks(Item par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		for(int i = 0; i < 2; i++)
		{
			par3List.add(new ItemStack(par1, 1, i));
		}
	}*/
}
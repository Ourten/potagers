package fr.things.potagers;

import cpw.mods.fml.client.registry.ClientRegistry;
import fr.things.potagers.render.TomatesRenderer;
import fr.things.potagers.tiles.TileEntityTomate;

public class ClientProxy extends CommonProxy
{
	@Override
	public void registerRenders()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTomate.class, new TomatesRenderer());
	}
}